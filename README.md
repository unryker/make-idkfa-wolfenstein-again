### Philosophy
---
- Runs in Vanilla Doom.

### Notes (spoilers, beware!)
---
- Medikit graphics are separated and can be loaded independently.
- All versions
	- DEHACKED is used for sprite replacements.
- MIWA.wad (Make IDKFA Wolfenstein Again)
	- ZMAPINFO for ZDoom to force the TITLEPIC to be used, for maximum compatibility with other Doom mods that replace the TITLEPIC.
	- Restores the original sprites of the blue smurfs.
	- Includes the Wolfenstein wall textures containing Nazi emblems.
	- Original image lumps of the map names have been included.
	- Modified MAP31 and MAP32 to properly replace Zombiemen with Nazis.
- NLM.wad (Nazi Lives Matter)
	- Breaks the sprites of the blue smurfs for max immersion.
	- Removes the Wolfenstein wall textures containing Nazi emblems.
	- Censored image lumps of the map names have been included.
	- Modified MAP31 and MAP32 to properly replace Nazis with Zombiemen.

### Installation
---
- UNCENSORING BFG DOOM 2
	- Chocolate Doom
		- Run with `-file miwa.wad meds_cross.wad -dehlump miwa.wad meds_cross.wad`. Or do `-file miwa.wad meds_cross.wad -deh miwa.deh meds.deh`.
	- Other source ports
		- Only load `miwa.wad` and `meds_cross.wad`.
- CENSORING OG DOOM 2
	- Chocolate Doom
		- Run with `-file nlm.wad meds_pills.wad -dehlump nlm.wad meds_pills.wad`. Or do `-file nlm.wad meds_pills.wad -deh nlm.deh meds.deh`.
	- Other source ports
		- Only load `nlm.wad` and `meds_pills.wad`.
- Note that files related to medikits can be loaded independently, so you can have uncensored/censored medikits only for either version of Doom.

### Compatibility
---
- PrBoom+ and its derivatives won't replace the Zombiemen with Nazis.
- Doesn't work with the fixed Nazi sprites from [Doom Minor Spritefixing Project.](https://www.doomworld.com/forum/topic/62403-doom-2-minor-sprite-fixing-project-v20-beta-1-release-updated-11822/) This is because of how the Nazi sprites are replaced in MIWA.wad/NLM.wad.